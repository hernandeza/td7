import Utile.Ut;

public class Date {


    //definir les attributs
    private int jour;
    private int mois;
    private int an;

    private static String[] moisLettres = {"janvier","fevrier","mars","avril", "mai" , "juin" , "juillet" , "aout", "septembre", "octobre", "novembre" , "decembre"};


    public Date(int jour, int mois, int an) {
        //TODO
        this.jour = jour;
        this.mois = mois;
        this.an = an;
    }
    public Date(Date d){
        //TODO
        this.jour = d.jour;
        this.mois = d.mois;
        this.an = d.an;
    }


    public String toString() {
        //TODO
       return this.jour + " " + moisLettres[this.mois] + " " + this.an;
    }

    private boolean anneeEstBissextile(){
        return Ut.estBissextile(this.an);
    }

    public int nbJoursMois () {

        if(anneeEstBissextile() && this.mois == 2){
            return 29;

        }else if (!anneeEstBissextile() && this.mois == 2){
            return 28;

        }

        if(this.mois == 4 || this.mois == 6 || this.mois == 9 || this.mois == 11) {
            return 30;

        }else {
            return 31;
        }
    }

    public void incrementer(){
        // Action : ajoute un jour à la date courante
        //TODO

        if(this.jour < nbJoursMois()){
            this.jour++;
        }else{
            this.jour = 1;
            this.mois ++;
            if (this.mois == 13) {
                this.mois = 1;
                this.an++;
            }
        }
    }

    public Date lendemain() {
        //TODO
        Date datlendemain = new Date(this);
        datlendemain.incrementer();

        return datlendemain;
    }

    public boolean egale (Date d){
        //TODO
        return this.jour == d.jour && this.mois == d.mois && this.an == d.an;
    }



    public boolean anterieure (Date d) {
        //TODO
        Date da = new Date(d);

        da.incrementer();
        return this.egale(da);
    }

    public boolean posterieure (Date d){
        //TODO
        Date da = new Date(this);

        da.incrementer();
        return d.egale(da);
    }

    public int ecart (Date d) {
        //TODO
        if(this.posterieure(d)) {
            return d.ecart(this);
        }
        int ecart =0 ;
        Date date = new Date(this);
        while( !date.egale(d)){
            ecart++;
            date.incrementer();
        }

        return ecart;


    }


}
